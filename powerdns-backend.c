#include <my_config.h>
#include <libmemcached/memcached.h>
#include <stdio.h>
#include <string.h>
#include <my_global.h>
#include <ctype.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <net/if.h>
#include <ifaddrs.h>
#include <sys/ioctl.h>
#include <time.h>
// Define your variables

#define DEBUG

#define MEMCACHE_HOST	"127.0.0.1"

#define DNS_TTL		60

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

struct qdns {
	char type;
	char qname[256];
	char qclass[3];
	char qtype[6];
	int id;
	char qip[256];
};

char *get_ip_str(const struct sockaddr *sa, char *s, size_t maxlen){
    switch(sa->sa_family) {
        case AF_INET:
            inet_ntop(AF_INET, &(((struct sockaddr_in *)sa)->sin_addr),
                    s, maxlen);
            break;

        case AF_INET6:
            inet_ntop(AF_INET6, &(((struct sockaddr_in6 *)sa)->sin6_addr),
                    s, maxlen);
            break;

        default:
            strncpy(s, "Unknown AF", maxlen);
            return NULL;
    }

    return s;
}

int isPrivate4(const char *ipstr){
        int i = 0;
        
        if ((strncmp(ipstr, "10.", 3) == 0) ||
            (strncmp(ipstr, "172.16.", 7) == 0) ||
            (strncmp(ipstr, "172.17.", 7) == 0) ||
            (strncmp(ipstr, "172.18.", 7) == 0) ||
            (strncmp(ipstr, "172.19.", 7) == 0) ||
            (strncmp(ipstr, "172.2.", 6) == 0) ||
            (strncmp(ipstr, "172.30.", 7) == 0) ||
            (strncmp(ipstr, "172.31.", 7) == 0) ||
            (strncmp(ipstr, "192.168.", 8) == 0)
        )
                i = 1;
        return i;
}

int main(){
	char helo[4];
	unsigned short int version;
	
	// memached vars
	memcached_server_st *servers = NULL;
	memcached_st *memc;
	memcached_return rc;
	
	// Until we get HELO\t1
	setbuf(stdout, NULL);
	fflush(stdin);
	do {
		scanf("%s\t%hu", helo, &version);
	    	#ifdef DEBUG
	    	fprintf(stderr, "DEBUG:[%s]:[%hu]\n", helo, version);
	    	#endif
	} while (version != 1);
	printf ("OK\tGeoPeer Proxy backend firing up\n");
	char trash;
	fflush(stdin);
	scanf("%c", &trash);
	fflush(stdin);

	memc = memcached_create(NULL);
	servers = memcached_server_list_append(servers, "localhost", 11211, &rc);
	rc = memcached_server_push(memc, servers);

	#ifdef	DEBUG
	if (rc == MEMCACHED_SUCCESS)		
		fprintf(stderr, "DEBUG: Memcached server successfully added\n");
	else{
		fprintf(stderr, "DEBUG: I couldn't add memcached server: %s\n", memcached_strerror(memc, rc));
	}
	#endif
	
	struct qdns input;
	memset(&input.qname, 0, sizeof(char)*256);
	memset(&input.qclass, 0, sizeof(char)*3);
	memset(&input.qtype, 0, sizeof(char)*6);
	memset(&input.qip, 0, sizeof(char)*256);
	while(scanf("%c%s%s%s%d%s", &input.type, input.qname, input.qclass, input.qtype, &input.id, input.qip)) {
		scanf("%c", &trash);
	    	fflush(stdin);

	    	int i;
	    	// All to lowcase
	    	input.type = tolower(input.type);
	    	for(i = 0; input.qname[i]; i++){
			input.qname[i] = tolower(input.qname[i]);
	    	}
	    	for(i = 0; input.qclass[i]; i++){
			input.qclass[i] = tolower(input.qclass[i]);
	    	}
	    	for(i = 0; input.qtype[i]; i++){
			input.qtype[i] = tolower(input.qtype[i]);
	    	}
	    	for(i = 0; input.qip[i]; i++){
			input.qip[i] = tolower(input.qip[i]);
	    	}
	    
	    	#ifdef	DEBUG
	    	fprintf(stderr, "DEBUG:[%c]\t[%s]\t[%s]\t[%s]\t[%d]\t[%s]\n", input.type, input.qname, input.qclass, input.qtype, input.id, input.qip);
	    	#endif
	    	if (!strlen(input.qip)){
			// There is no query to look for in i
			printf ("LOG\tPowerDNS sent unparseable line\n");
			printf ("FAIL\n");
	    	}
	    	else{
			char key[256];
			char value [256];
                        		  
                        if (((strncmp(input.qtype, "a", 1) == 0) || (strncmp(input.qtype, "soa", 3) == 0) || (strncmp(input.qtype, "any", 3)) == 0)){
                                char str[15];
                                char *retrieved_value;
                                size_t value_length;
                                uint32_t flags;
                                memset(key, '\0', sizeof(key));
                                memset(value, '\0', sizeof(value));
                                memset(str, '\0', sizeof(str));

                                strcpy(key, "proxy_");
                                strcat(key, input.qname);
                                sprintf(str, "_%d", (int)1);
                                strcat(key, str);
                                
                                #ifdef	DEBUG
                                fprintf(stderr, "DEBUG:Looking key %s in MEMCACHED\n", key);
                                #endif
                                retrieved_value = memcached_get(memc, key, strlen(key), &value_length, &flags, &rc);
                                if (rc == MEMCACHED_SUCCESS) {
                                        #ifdef	DEBUG
                                        fprintf(stderr, "DEBUG:DATA in MEMCACHED\n");
                                        #endif

                                        if (((strncmp(input.qtype, "soa", 3) == 0) || (strncmp(input.qtype, "any", 3)) == 0)){
                                                #ifdef	DEBUG
                                                fprintf(stderr, "DEBUG:DATA\t%s\t%s\tSOA\t3600\t-1\tsupport.geopeer.org. %s. 2008080300 1800 3600 604800 3600\n", input.qname, input.qclass, input.qname);
                                                #endif
                                                printf ("DATA\t%s\t%s\tSOA\t3600\t-1\tsupport.geopeer.org. %s. 2008080300 1800 3600 604800 3600\n", input.qname, input.qclass, input.qname);                                        
                                        }

                                        if (((strncmp(input.qtype, "a", 1) == 0) || (strncmp(input.qtype, "any", 3)) == 0)){
                                            
                                                short int count = 2;
                                                do {
                                                        #ifdef	DEBUG
                                                                fprintf(stderr, "DEBUG: memcached key: %s exists with value '%s'\n", key, retrieved_value);
                                                        #endif
                                                        if (isPrivate4((char *)retrieved_value)){
                                                                #ifdef	DEBUG
                                                                fprintf(stderr, "DEBUG:DATA\t%s\tIN\tA\t%i\t1\t%s\n", input.qname, DNS_TTL, (char *)retrieved_value);
                                                                #endif
                                                                printf ("DATA\t%s\tIN\tA\t%i\t1\t%s\n", input.qname, DNS_TTL, (char *)retrieved_value);
                                                        }
                                                        else{
                                                                #ifdef	DEBUG
                                                                fprintf(stderr, "DEBUG:NODATA\t%s\tIN\tA\t%i\t1\t%s\n", input.qname, DNS_TTL, (char *)retrieved_value);
                                                                #endif
                                                        }
                                                        free(retrieved_value);
                                                        
                                                        memset(key, '\0', sizeof(key));
                                                        strcpy(key, "proxy_");
                                                        strcat(key, input.qname);
                                                        strcat(key, "_");
                                                        sprintf(str, "_%d", (int)count);
                                                        strcat(key, str);
                                                        count++;
                                                        retrieved_value = memcached_get(memc, key, strlen(key), &value_length, &flags, &rc);
                                                } while(rc == MEMCACHED_SUCCESS);
                                        }
                                }
                                else{
                                        struct addrinfo hints, *res, *p;
                                        int status;
                                        short unsigned c;
                                        char *ipstr;
                                        struct timespec ts;
                                        ts.tv_sec = 1;
                                        ts.tv_nsec = 1000000;
                                        nanosleep(&ts, NULL);
                                        ipstr = malloc(sizeof(char) * INET6_ADDRSTRLEN);
                                        memset(&hints, 0, sizeof hints);
                                        memset(ipstr, 0, sizeof(char) * INET6_ADDRSTRLEN);
                                        hints.ai_family = AF_UNSPEC; // AF_INET or AF_INET6 to force version
                                        hints.ai_socktype = SOCK_STREAM;
                                        hints.ai_flags |= AI_CANONNAME;
					
					for (c = 0; status != 0 || c < 5; c++)
						status = getaddrinfo(input.qname, NULL, &hints, &res);

                                        if (status != 0) {
                                                #ifdef	DEBUG
                                                fprintf(stderr, "DEBUG: getaddrinfo: %s\n", gai_strerror(status));
                                                #endif
                                                printf ("LOG\tCouldnt resolve %s\n", input.qname);
                                                printf ("FAIL\n");
                                        }
                                        else {           
                                                short int count = 1;
                                                for(p = res;p != NULL; p = p->ai_next, count++) {
                                                        //void *addr;

                                                        // get the pointer to the address itself,
                                                        // different fields in IPv4 and IPv6:
                                                        if (p->ai_family == AF_INET) { // IPv4
                                                                struct sockaddr_in *ipv4 = (struct sockaddr_in *)p->ai_addr;
                                                                //addr = &(ipv4->sin_addr);
                                                                
                                                                // convert the IP to a string and print it:
                                                                ipstr = get_ip_str((struct sockaddr *)ipv4, ipstr, sizeof(char)*INET6_ADDRSTRLEN);
                                                                //inet_ntop(p->ai_family, addr, ipstr, sizeof(char)*INET6_ADDRSTRLEN);
                                                                
                                                                #ifdef DEBUG
                                                                fprintf(stderr, "DEBUG: %s has IP %s\n", input.qname, ipstr);
                                                                #endif
                                                                
                                                                memset(key, '\0', sizeof(key));
                                                                strcpy(key, "proxy_");
                                                                strcat(key, input.qname);
                                                                sprintf(str, "_%d", (int)count);
                                                                strcat(key, str);

                                                                if (isPrivate4(ipstr)){
                                                                        #ifdef DEBUG
                                                                        fprintf(stderr, "DEBUG: %s is identified as private\n", ipstr);
                                                                        #endif
                                                                        struct ifaddrs *addrs, *iap;
                                                                        struct sockaddr_in *sa, *sn;
                                                                        
                                                                        struct sockaddr_in client_ip;
                                                                        inet_aton(input.qip, &client_ip.sin_addr);
                                        
                                                                        getifaddrs(&addrs);
                                                                        for (iap = addrs; iap != NULL; iap = iap->ifa_next) {
                                                                                if (iap->ifa_addr && (iap->ifa_flags & IFF_UP) && iap->ifa_addr->sa_family == AF_INET) {
                                                                                        char *if_ip, *if_mask;
                                                                                        if_ip = malloc(sizeof(char) * INET6_ADDRSTRLEN);
                                                                                        memset(if_ip, 0, sizeof(char) * INET6_ADDRSTRLEN);
                                                                                        if_mask = malloc(sizeof(char) * INET6_ADDRSTRLEN);
                                                                                        memset(if_mask, 0, sizeof(char) * INET6_ADDRSTRLEN);
                                                                        
                                                                                        sa = (struct sockaddr_in *)(iap->ifa_addr);
                                                                                        sn = (struct sockaddr_in *)(iap->ifa_netmask);
                                                                                        inet_ntop(iap->ifa_addr->sa_family, (void *)&(sa->sin_addr), if_ip, sizeof(char)*INET6_ADDRSTRLEN);
                                                                                        inet_ntop(iap->ifa_netmask->sa_family, (void *)&(sn->sin_addr), if_mask, sizeof(char)*INET6_ADDRSTRLEN);
                                                                                        #ifdef DEBUG
                                                                                        fprintf(stderr, "DEBUG:%s %s %s\n", iap->ifa_name, if_ip, if_mask);
                                                                                        #endif
                                                                                        
                                                                                        if (((sa->sin_addr).s_addr & (sn->sin_addr).s_addr) ==
                                                                                            ((client_ip.sin_addr).s_addr & (sn->sin_addr).s_addr)
                                                                                        ){
                                                                                                #ifdef DEBUG
                                                                                                fprintf(stderr, "DEBUG: belongs\n");
                                                                                                #endif
                                                                                                strcpy(ipstr, if_ip);
                                                                                                free(if_ip);
                                                                                                free(if_mask);
                                                                                                break;
                                                                                        }
                                                                                        
                                                                                        free(if_ip);
                                                                                        free(if_mask);
                                                                                }
                                                                        }
                                                                        
                                                                        freeifaddrs(addrs);
                                                                        
                                                                        if (((strncmp(input.qtype, "soa", 3) == 0) || (strncmp(input.qtype, "any", 3)) == 0)){
                                                                                #ifdef	DEBUG
                                                                                fprintf(stderr, "DEBUG:DATA\t%s\t%s\tSOA\t3600\t-1\tsupport.geopeer.org. %s. 2008080300 1800 3600 604800 3600\n", input.qname, input.qclass, input.qname);
                                                                                #endif
                                                                                printf ("DATA\t%s\t%s\tSOA\t3600\t-1\tsupport.geopeer.org. %s. 2008080300 1800 3600 604800 3600\n", input.qname, input.qclass, input.qname);
                                                                        }

                                                                        if (((strncmp(input.qtype, "a", 1) == 0) || (strncmp(input.qtype, "any", 3)) == 0)){                                
                                                                                #ifdef DEBUG
                                                                                fprintf(stderr, "DEBUG:DATA\t%s\tIN\tA\t%i\t1\t%s\n", input.qname, DNS_TTL, ipstr);
                                                                                #endif
                                                                                printf ("DATA\t%s\tIN\tA\t%i\t1\t%s\n", input.qname, DNS_TTL, ipstr);
                                                                                rc = memcached_set(memc, key, strlen(key), ipstr, strlen(ipstr), (time_t)60, (uint32_t)0);

                                                                                #ifdef	DEBUG
                                                                                if (rc == MEMCACHED_SUCCESS) {
                                                                                        fprintf(stderr, "DEBUG: memcached key: %s set with value '%s'\n", key, ipstr);
                                                                                }
                                                                                else{
                                                                                        fprintf(stderr, "DEBUG: memcached key: %s was not able to be set into memcached\n", key);
                                                                                }
                                                                                #endif

                                                                        }
                                                                }
                                                                else{
                                                                    #ifdef DEBUG
                                                                    fprintf(stderr, "DEBUG:NODATA\t%s\tIN\tA\t%i\t1\t%s\n", input.qname, DNS_TTL, ipstr);
                                                                    #endif
                                                                    //rc = memcached_set(memc, key, strlen(key), ipstr, strlen(ipstr), (time_t)33660, (uint32_t)0);
                                                                }
                                                    
                                                        } 
                                                /*	else { // IPv6
                                                                struct sockaddr_in6 *ipv6 = (struct sockaddr_in6 *)p->ai_addr;
                                                                addr = &(ipv6->sin6_addr);
                                                                
                                                                // convert the IP to a string and print it:
                                                                inet_ntop(p->ai_family, addr, ipstr, sizeof ipstr);
                                                        } */
                                                }
                                                
                                                
                                                freeaddrinfo(res); // free the linked list
                                        }
                                        
                                        free (ipstr);
                                }
                        }
		    
                }
                #ifdef DEBUG
                fprintf(stderr, "DEBUG:END\n");
                #endif
                printf ("END\n");
                fflush(stdin);
                fflush(stdout);
        }
	return 0;
}
