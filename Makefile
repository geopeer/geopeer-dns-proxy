PROJECT=geopeer-powerdns-proxy
SOURCES=powerdns-backend.c
LIBRARY=nope
INCPATHS=/usr/include/mysql
LIBPATHS=/usr/lib64/mysql
LDFLAGS=-lmysqlclient -lmemcached -lmemcachedutil
CFLAGS=-c -Wall $(mysql_config --cflags)
CC=gcc
 
# ------------ MAGIC BEGINS HERE -------------
 
# Automatic generation of some important lists
OBJECTS=$(SOURCES:.c=.o)
INCFLAGS=$(foreach TMP,$(INCPATHS),-I$(TMP)) $(mysql_config --include)
LIBFLAGS=$(foreach TMP,$(LIBPATHS),-L$(TMP)) $(mysql_config  --libs)
 
# Set up the output file names for the different output types
ifeq "$(LIBRARY)" "shared"
    BINARY=lib$(PROJECT).so
    LDFLAGS += -shared
else ifeq "$(LIBRARY)" "static"
    BINARY=lib$(PROJECT).a
else
    BINARY=$(PROJECT)
endif
 
all: $(SOURCES) $(BINARY)
 
$(BINARY): $(OBJECTS)
    # Link the object files, or archive into a static library
    ifeq "$(LIBRARY)" "static"
	ar rcs $(BINARY) $(OBJECTS)
    else
	$(CC) $(LIBFLAGS) $(OBJECTS) $(LDFLAGS) -o $@
    endif
 
.c.o:
	$(CC) $(INCFLAGS) $(CFLAGS) -O3 -fPIC $< -o $@
 
distclean: clean
	rm -f $(BINARY)
 
clean:
	rm -f $(OBJECTS)
